<#ftl encoding="UTF-8"/>
<#include "base.ftl"/>
<#macro content>
<div class="container">
    <div class="row col-lg-10 offset-1" style="margin-top: 40px;background-color: rgba(168,168,168,0.9)">
        <div class="col-lg-12">
            <div class="title">
                <#if user??>
                <h3 class="title__main">
                    Профиль:
                </h3>
                <h4 class="title__text">
                    Имя:${name}<br>
                    Фамилия:${surname}<br>
                    Рейтинг:${rating}<br>
                    О себе:${about}<br><br>
                    <a href="logout" class="button7">Выйти</a>
                </h4></div>
            <#else>
                <h3 class="title__main">
                    Пожалуйста, войдите или зарегистрируйтесь, чтобы увидеть содержимое страницы.<br>
                </h3>
                    <a href="loginPage" class="button7">Вход</a><br><br><br><br>
                    <a href="registration" class="buttonReg">Регистрация</a>
            </#if>
        </div>
    </div>
</div>
</#macro>
<@main/>
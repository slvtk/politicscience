<#ftl encoding="UTF-8"/>
<#include "base.ftl"/>
<#macro content>
    <#if user??>
        <form action="test" method="post" class="justify-content-center">
            <ul class="list-group">
                <#list questions as question>
                    <li class="list-group-item">${question}<br>
                        <label>
                            <input type="text" name="answer" class="form-control" placeholder="Ваш ответ">
                        </label>
                    </li>
                </#list>
                <input type="submit" value="Узнать результат" class="button7"><br><br>
            </ul>
        </form>
    <#else>
        <div class="container">
            <div class="row col-lg-10 offset-1" style="margin-top: 40px;background-color: rgba(168,168,168,0.9)">
                <div class="col-lg-12">
                    <div class="title">
                        <h3 class="title__main">
                            Пожалуйста, войдите или зарегистрируйтесь, чтобы увидеть содержимое страницы.<br>
                        </h3>
                        <a href="loginPage" class="button7">Вход</a><br><br><br><br>
                        <a href="registration" class="buttonReg">Регистрация</a>
                    </div>
                </div>
            </div>
        </div>
    </#if>
</#macro>
<@main/>
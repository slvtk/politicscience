<#ftl encoding="UTF-8"/>
<#include "base.ftl"/>
<#macro content>
    <#if user??>
        <ul class="list-group">
            <#list users as user>
                <li class="list-group-item"><b>${user_index+1}.${user}</b></li>
            </#list>
        </ul>
    <#else>
        <div class="container">
            <div class="row col-lg-10 offset-1" style="margin-top: 40px;background-color: rgba(168,168,168,0.9)">
                <div class="col-lg-12">
                    <div class="title">
                        <h3 class="title__main">
                            Пожалуйста, войдите или зарегистрируйтесь, чтобы увидеть содержимое страницы.<br>
                        </h3>
                        <a href="loginPage" class="button7">Вход</a><br><br><br><br>
                        <a href="registration" class="buttonReg">Регистрация</a>
                    </div>
                </div>
            </div>
        </div>
    </#if>
</#macro>
<@main/>
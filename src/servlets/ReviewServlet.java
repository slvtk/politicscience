package servlets;

import DAO.DAOReviewServlet;
import entities.User;
import helpers.Helpers;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class ReviewServlet extends HttpServlet {
    DAOReviewServlet daoReviewServlet = new DAOReviewServlet();

    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("text/html");
        resp.setCharacterEncoding("UTF-8");
        req.setCharacterEncoding("UTF-8");
        Map<String, Object> root = new HashMap<>();
        HttpSession session = req.getSession();
        root.put("reviews", daoReviewServlet.get());
        root.put("user", (User) session.getAttribute("user"));
        Helpers.render(req, resp, "reviews.ftl", root);
    }

    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        resp.setContentType("text/html");
        resp.setCharacterEncoding("UTF-8");
        req.setCharacterEncoding("UTF-8");
        DAOReviewServlet daoReviewServlet = new DAOReviewServlet();
        daoReviewServlet.save(Integer.parseInt(User.getInstance().getId()), req.getParameter("review"), Integer.parseInt(req.getParameter("test")));
        resp.sendRedirect("reviews");
    }
}

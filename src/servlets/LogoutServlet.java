package servlets;

import entities.User;

import javax.servlet.http.*;
import java.io.IOException;

public class LogoutServlet extends HttpServlet {

    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        User.getInstance().clear();
        HttpSession session = req.getSession();
        Cookie cookie = new Cookie("user", "");
        cookie.setMaxAge(0);
        resp.addCookie(cookie);
        session.invalidate();
        resp.sendRedirect("loginPage");
    }
}

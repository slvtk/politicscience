package servlets;

import DAO.DAOLoginServlet;
import entities.User;

import javax.servlet.*;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.IOException;

public class CookieFilter implements Filter {

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {

        HttpServletRequest request = (HttpServletRequest) servletRequest;
        Cookie[] cookies = request.getCookies();
        String cookieName = "user";
        Cookie cookie = null;
        if (cookies != null) {
            for (Cookie c : cookies) {
                if (cookieName.equals(c.getName())) {
                    cookie = c;
                    break;
                }
            }
            if (cookie != null) {
                DAOLoginServlet daoLoginServlet = new DAOLoginServlet();
                User user = daoLoginServlet.getByID(cookie.getValue());
                HttpSession session = request.getSession();
                session.setAttribute("user", user);
            }
        }
        filterChain.doFilter(servletRequest, servletResponse);
    }
}


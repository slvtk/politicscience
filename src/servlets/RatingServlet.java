package servlets;

import DAO.DAORatingServlet;
import entities.User;
import helpers.Helpers;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class RatingServlet extends HttpServlet {

    DAORatingServlet daoRatingServlet = new DAORatingServlet();

    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        resp.setContentType("text/html");
        resp.setCharacterEncoding("UTF-8");
        req.setCharacterEncoding("UTF-8");
        Map<String, Object> root = new HashMap<>();
        HttpSession session = req.getSession();
        User user = (User) session.getAttribute("user");
        List<String> users = daoRatingServlet.get();
        root.put("users", users);
        root.put("user", user);
        Helpers.render(req, resp, "rating.ftl", root);
    }
}

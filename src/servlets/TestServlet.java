package servlets;

import DAO.DAOTestServlet;
import entities.User;
import freemarker.template.Configuration;
import freemarker.template.TemplateExceptionHandler;
import helpers.Helpers;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class TestServlet extends HttpServlet {
    private DAOTestServlet daoTestServlet = new DAOTestServlet();

    @Override
    public void init() {
        Configuration cfg = new Configuration(Configuration.VERSION_2_3_22);
        cfg.setServletContextForTemplateLoading(getServletContext(),
                "/WEB-INF/templates/");
        cfg.setTemplateExceptionHandler(TemplateExceptionHandler.HTML_DEBUG_HANDLER);
        getServletContext().setAttribute("cfg", cfg);
    }

    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        resp.setCharacterEncoding("UTF-8");
        req.setCharacterEncoding("UTF-8");
        HttpSession session = req.getSession();
        session.setAttribute("testid", req.getParameter("id"));
        User user = (User) session.getAttribute("user");
        List<String> questions;
        Map<String, Object> root = new HashMap<>();
        questions = daoTestServlet.get(req.getParameter("id"));
        root.put("questions", questions);
        root.put("user", user);
        Helpers.render(req, resp, "test.ftl", root);

    }

    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        resp.setCharacterEncoding("UTF-8");
        req.setCharacterEncoding("UTF-8");
        HttpSession session = req.getSession();
        User user = (User) session.getAttribute("user");
        int rating;
        String[] params = req.getParameterValues("answer");
        String[] rightAnswers = new String[5];
        int result = 0;
        ResultSet resultSet = daoTestServlet.getByTestId(session.getAttribute("testid").toString());
        try {
            int i = 0;
            while (resultSet.next()) {
                rightAnswers[i] = resultSet.getString("right_answer");
                i++;
            }
            i = 0;
            for (String answer :
                    params) {
                if (answer.equals(rightAnswers[i])) {
                    result = result + 1;
                }
                i++;
            }

            rating = 25 * result;
            daoTestServlet.update(rating, user.getId());
            user.setRating(user.getRating() + rating);
            resp.sendRedirect("profile");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}

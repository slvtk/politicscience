package servlets;

import DAO.DAOLoginServlet;
import entities.User;

import javax.servlet.http.*;
import java.io.IOException;

public class LoginServlet extends HttpServlet {

    DAOLoginServlet daoLoginServlet = new DAOLoginServlet();

    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {

        resp.setCharacterEncoding("UTF-8");
        req.setCharacterEncoding("UTF-8");

        HttpSession session = req.getSession();
        String login = req.getParameter("login");
        String password = req.getParameter("password");

        if (User.getInstance().getId() == null) {
            daoLoginServlet.getByLogin(login, password);
        } else {
            daoLoginServlet.getByID(User.getInstance().getId());
        }
        if (User.getInstance().getId() != null) {
            session.setAttribute("user", User.getInstance());
        }

        if ((session.getAttribute("user") != null) && (req.getParameter("remember") != null)) {
            Cookie cookie = new Cookie("user", User.getInstance().getId());
            cookie.setMaxAge(3600);
            resp.addCookie(cookie);
        }
        resp.sendRedirect("profile");
    }

    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        resp.setCharacterEncoding("UTF-8");
        req.setCharacterEncoding("UTF-8");
        String name = req.getParameter("name");
        String surname = req.getParameter("surname");
        String about = req.getParameter("about");
        String login = req.getParameter("login");
        String password = req.getParameter("password");
        daoLoginServlet.save(name, surname, about, login, password);
        resp.sendRedirect("loginPage");
    }
}


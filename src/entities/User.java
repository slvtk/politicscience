package entities;

public class User {

    private static User instance;
    private String id = null;
    private String name = null;
    private String surname = null;
    private int rating = 0;

    public int getRating() {
        return rating;
    }

    public void setRating(int rating) {
        this.rating = rating;
    }

    public String getAbout() {
        return about;
    }

    public void setAbout(String about) {
        this.about = about;
    }

    private String about;

    private boolean login = false;

    private User() {
    }

    public static User getInstance() {
        if (instance == null) {
            instance = new User();
        }
        return instance;
    }

    public void clear() {
        this.id = null;
        this.name = null;
        this.surname = null;
        this.about = null;
        this.rating = 0;
        this.login = false;
    }


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public boolean isLogin() {
        return login;
    }

    public void setLogin() {
        this.login = true;
    }

    @Override
    public String toString() {
        return "<h3>ID:</h3><p>" + this.id + "</p><h3>Name:</h3><p>" + this.name + "</p><h3>Surname:</h3><p>" + this.surname + "</p><h3>Rating:</h3><p>" + this.rating + "</p><h3>About:</h3><p>" + this.about + ".</p>";
    }
}
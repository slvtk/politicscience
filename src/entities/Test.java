package entities;

import java.util.ArrayList;

public class Test {

    private String id;
    private String title;
    private ArrayList questions;
    private ArrayList answers;
    private ArrayList rightAnswers;
    private ArrayList userAnswers;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public ArrayList getQuestions() {
        return questions;
    }

    public void setQuestions(ArrayList questions) {
        this.questions = questions;
    }

    public ArrayList getAnswers() {
        return answers;
    }

    public void setAnswers(ArrayList answers) {
        this.answers = answers;
    }

    public ArrayList getRightAnswers() {
        return rightAnswers;
    }

    public void setRightAnswers(ArrayList rightAnswers) {
        this.rightAnswers = rightAnswers;
    }

    public ArrayList getUserAnswers() {
        return userAnswers;
    }

    public void setUserAnswers(ArrayList userAnswers) {
        this.userAnswers = userAnswers;
    }

}

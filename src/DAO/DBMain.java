package DAO;

import java.sql.*;

public class DBMain {
    private Connection connection;

    public DBMain(){
        try {
            Class.forName("com.mysql.jdbc.Driver");
            connection=DriverManager.getConnection("jdbc:mysql://localhost:3306/db_politicscience","root","6270");
        }catch (SQLException | ClassNotFoundException e){
            e.printStackTrace();
        }
    }

    public Connection getConnection() {
        return connection;
    }
}


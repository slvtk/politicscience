package DAO;

import entities.User;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class DAOLoginServlet {

    public void getByLogin(String login, String password) {
        try {
            DBMain dbMain = new DBMain();
            String query = "select * from users where login=? and password=?";
            PreparedStatement preparedStatement = dbMain.getConnection().prepareStatement(query);
            preparedStatement.setString(1, login);
            preparedStatement.setString(2, password);
            getUserFields(preparedStatement);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public User getByID(String id) {
        try {
            DBMain dbMain = new DBMain();
            String query = "select * from users where id=?";
            PreparedStatement preparedStatement = dbMain.getConnection().prepareStatement(query);
            preparedStatement.setString(1, id);
            getUserFields(preparedStatement);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return User.getInstance();
    }

    private void getUserFields(PreparedStatement preparedStatement) throws SQLException {
        ResultSet resultSet = preparedStatement.executeQuery();
        while (resultSet.next()) {
            User.getInstance().setId(resultSet.getString("id"));
            User.getInstance().setName(resultSet.getString("name"));
            User.getInstance().setSurname(resultSet.getString("surname"));
            User.getInstance().setRating(resultSet.getInt("rating"));
            User.getInstance().setAbout(resultSet.getString("about"));
        }
    }

    public void save(String name, String surname, String about, String login, String password) {
        DBMain dbMain = new DBMain();
        try {

            String query = "insert into users (name,surname,rating,about,login, password) values(?,?,0,?,?,?)";
            PreparedStatement preparedStatement = dbMain.getConnection().prepareStatement(query);
            preparedStatement.setString(1, name);
            preparedStatement.setString(2, surname);
            preparedStatement.setString(3, about);
            preparedStatement.setString(4, login);
            preparedStatement.setString(5, password);
            preparedStatement.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}

package DAO;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class DAOTestServlet {

    public List<String> get(String id) {
        DBMain dbMain = new DBMain();
        List<String> questions = new ArrayList<>();
        try {
            String query="select * from questions where test_id=?";
            PreparedStatement preparedStatement = dbMain.getConnection().prepareStatement(query);
            preparedStatement.setString(1,id);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                questions.add("<b>" + resultSet.getString("question") + "</b><br>1." + resultSet.getString("first_variant") + " 2." + resultSet.getString("second_variant") + " 3." + resultSet.getString("third_variant"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return questions;
    }

    public ResultSet getByTestId(String testid) {
        ResultSet resultSet = null;
        try {
            DBMain dbMain = new DBMain();
            String query="select right_answer from questions where test_id=?";
            PreparedStatement preparedStatement = dbMain.getConnection().prepareStatement(query);
            preparedStatement.setString(1,testid);
            resultSet =preparedStatement.executeQuery();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return resultSet;
    }

    public void update(int rating, String id) {
        DBMain dbMain = new DBMain();
        try {
            String query="UPDATE users SET rating=rating+? WHERE id=?";
            PreparedStatement preparedStatement = dbMain.getConnection().prepareStatement(query);
            preparedStatement.setInt(1,rating);
            preparedStatement.setString(2,id);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}

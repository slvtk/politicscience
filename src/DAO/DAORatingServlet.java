package DAO;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class DAORatingServlet {
    public List<String> get(){
        List<String> users = new ArrayList<>();
        DBMain dbMain = new DBMain();
        try {
            String query="select * from users order by rating desc";
            PreparedStatement preparedStatement = dbMain.getConnection().prepareStatement(query);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()){
                users.add(resultSet.getString("name")+" "+resultSet.getString("surname"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return users;
    }
}

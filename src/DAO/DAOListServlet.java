package DAO;

import entities.Test;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class DAOListServlet {

    public List<String> get() {
        DBMain dbMain = new DBMain();
        List<String> tests = new ArrayList<>();
        try {
            String query = "select * from tests order by id";
            PreparedStatement preparedStatement = dbMain.getConnection().prepareStatement(query);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                Test test = new Test();
                test.setId(resultSet.getString("id"));
                test.setTitle(resultSet.getString("title"));
                tests.add(test.getId() + ". " + test.getTitle());
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return tests;
    }
}

package DAO;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class DAOReviewServlet {

    public List<String> get() {
        List<String> reviews = new ArrayList<>();
        DBMain dbMain = new DBMain();
        try {
            String query = "select u.name,u.surname,t.title,r.time,r.review from users u join reviews r on u.id = r.user_id join tests t on r.tests_id = t.id ";
            PreparedStatement preparedStatement = dbMain.getConnection().prepareStatement(query);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                reviews.add(resultSet.getDate("time") + " " + resultSet.getTime("time") + " " + resultSet.getString("name") + " " + resultSet.getString("surname") + " пишет о тесте \"" + resultSet.getString("title") + "\" :  " + resultSet.getString("review"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return reviews;
    }

    public void save(int userid, String review, int testid) {
        try {
            DBMain dbMain = new DBMain();
            String query = "insert into reviews (user_id,review,tests_id,time) values (?,?,?,(now()))";
            PreparedStatement preparedStatement = dbMain.getConnection().prepareStatement(query);
            preparedStatement.setInt(1, userid);
            preparedStatement.setString(2, review);
            preparedStatement.setInt(3, testid);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }
}

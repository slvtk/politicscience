<#ftl encoding="UTF-8"/>
<#include "base.ftl"/>
<#macro content>
    <#if user??>
        <ul class="list-group">
            <#list reviews as review>
                <li class="list-group-item">${review}</li>
            </#list>
        </ul>
        <section id="login" class="review">
            <div class="container">
                <div class="row">
                    <div class="col-lg-10 offset-1">
                        <div class="login__form ">
                            <form  action="reviews" method="post">
                                <div class="form-group">
                                    <div class="col-lg-10 offset-1">
                                        <label for="exampleInputEmail1">Номер теста</label>
                                        <input type="text" name="test" class="form-control" placeholder="Номер теста">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-lg-10 offset-1">
                                        <label for="exampleInputEmail1">Комментарий</label>
                                        <input type="text" name="review" class="form-control" placeholder="Комментарий">
                                    </div>
                                </div>
                                <input type="submit" value="Отправить" class="button7" >
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    <#else>
        <div class="container">
            <div class="row col-lg-10 offset-1" style="margin-top: 40px;background-color: rgba(168,168,168,0.9)">
                <div class="col-lg-12">
                    <div class="title">
                        <h3 class="title__main">
                            Пожалуйста, войдите или зарегистрируйтесь, чтобы увидеть содержимое страницы.<br>
                        </h3>
                        <a href="loginPage" class="button7">Вход</a><br><br><br><br>
                        <a href="registration" class="buttonReg">Регистрация</a>
                    </div>
                </div>
            </div>
        </div>
    </#if>
</#macro>
<@main/>
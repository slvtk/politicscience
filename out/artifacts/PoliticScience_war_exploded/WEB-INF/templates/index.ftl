<#ftl encoding="UTF-8"/>
<#include "base.ftl"/>

<#macro content>
<section id="main" class="main">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="title">
                    <h2 class="title__main">
                        Политические тесты
                    </h2>
                    <p class="title__text">
                        Сайт предназначен для обучения политической науке.<br>
                        У пользователя есть возможность : <br>
                        Проходить тематические тесты, которые разбиты на блоки;<br>
                        Отслеживать свой прогресс прохождения тестов;<br>
                        Состязаться с другими Пользователями, по рейтингу,
                        который увеличивается в зависимости от количества правильных ответов;<br>
                        Оставлять комментарии к тестам.
                    </p>
                    <#if user??>
                        <a href="profile" class="button7" >Профиль</a>
                        <#else>
                            <a href="loginPage" class="button7" >Вход</a>
                    </#if>
                </div>
            </div>
        </div>
    </div>
    </section>
</#macro>
<@main/>
<#ftl encoding="UTF-8"/>
<#include "base.ftl"/>
<#macro content>
<section id="login" class="login">
    <div class="container">
        <div class="row">
            <div class="col-lg-10 offset-1">
                <div class="login__form">
                    <form action="login">
                        <div class="form-group">
                            <div class="col-lg-10 offset-1">
                                <label for="exampleInputEmail1">Логин</label>
                                <input type="text" name="login" class="form-control" id="exampleInputEmail1"
                                       aria-describedby="emailHelp" placeholder="Введите логин">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-lg-10 offset-1">
                                <label for="exampleInputPassword1">Пароль</label>
                                <input type="password" name="password" class="form-control" id="exampleInputPassword1"
                                       placeholder="Введите пароль">
                            </div>
                        </div>
                        <div class="form-group form-check">
                            <input type="checkbox" name="remember" value="true" class="form-check-input" id="exampleCheck1">
                            <label class="form-check-label" for="exampleCheck1">Запомнить меня</label>
                        </div>
                        <input type="submit" value="Вход" class="button7"><br><br>
                        <a href="registration" class="buttonReg">Регистрация</a>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>
</#macro>
<@main/>
<#ftl encoding="UTF-8"/>
<#include "base.ftl"/>
<#macro content>
<section id="login" class="login">
    <div class="container">
        <div class="row">
            <div class="col-lg-10 offset-1">
                <div class="login__form">
                    <form  action="login" method="post">
                        <div class="form-group">
                            <div class="col-lg-10 offset-1">
                                <label for="exampleInputEmail1">ВВЕДИТЕ ДАННЫЕ</label>
                                <input type="text" name="login" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Логин">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-lg-10 offset-1">
                                <input type="text" name="password" class="form-control" id="exampleInputPassword1" placeholder="Пароль">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-lg-10 offset-1">
                                <input type="text" name="name" class="form-control" id="exampleInputPhone" placeholder="Имя">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-lg-10 offset-1">
                                <input type="text" name="surname" class="form-control" id="exampleInputInfo" placeholder="Фамилия">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-lg-10 offset-1">
                                <input type="text" name="about" class="form-control" id="exampleInputInfo" placeholder="О себе">
                            </div>
                        </div>

                        <input type="submit" value="Регистрация" class="button7" >
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>
</#macro>
<@main/>
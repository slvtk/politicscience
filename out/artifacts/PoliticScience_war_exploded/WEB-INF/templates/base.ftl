<#ftl encoding="UTF-8"/>
<#macro content></#macro>
<#macro main>
<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>PoliticScience</title>
    <link href="https://fonts.googleapis.com/css?family=Merriweather:400,900&display=swap&subset=cyrillic"
          rel="stylesheet">
    <link rel="stylesheet" href="/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="/css/stylesheet.css">
</head>
<body>
<nav class="navbar navbar-expand-lg navbar-light bg-light body">
    <div class="col-lg-3 image">
        <img src="/img/logo200.png" alt="logo" class="logo">
    </div>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarNav" >
        <ul class="navbar-nav" style="margin-left: auto">
            <li class="nav-item">
                <a class="nav-link" href="indexPage">Главная</a>
            </li>
            <#if user??>
                <li class="nav-item">
                    <a class="nav-link" href="profile">Профиль</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="list">Тесты</a>
                </li>
            <#else>
                <li class="nav-item">
                    <a class="nav-link" href="loginPage">Вход</a>
                </li>
            </#if>
            <li class="nav-item">
                <a class="nav-link" href="reviews">Обсуждение</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="rating">Лидеры</a>
            </li>
        </ul>
    </div>
</nav>
<@content/>
</body>
</html>
</#macro>
